var chalk = require("chalk");
var _ = require("underscore");

var module = module.exports;

module.printBrands = function(brands){

	_.each(brands, function(brandData, i){
		console.log("\t"+(i+1)+". "+chalk.bold.blue("Brand name: " + brandData.description));
        console.log("\t"+(i+1)+". "+chalk.bold.blue("Brand image_url: " + brandData.image_url));
	    console.log("\t=============================================")
	    _.each(brandData.rewards, function(r, j){
	     	module.printRewardData(r, "\t\t", j + 1);
	    });
	    console.log("\n");
	    console.log("\n");
	});
      
};

module.printRewardData = function(r, tabs, index){
	/*{"description":"Xbox Digital Gift Card $15","sku":"XBOX1-E-1500-STD","currency_code":"USD","available":false,"type":"reward","countries":["US"],"is_variable":false,"denomination":"1500"}*/	
	console.log(tabs+index+". ");
	for(var k in r){
		var v = r[k];
		if(v === false){
			console.log(tabs + chalk.cyan(k) + " = " + chalk.red(v));
		}else{
			console.log(tabs + chalk.cyan(k) + " = " + chalk.green(v));
		}
		
	}	

	console.log("\t"+"---------------------------------------------");

};


