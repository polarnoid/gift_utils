/**
 * Created by arahman on 6/18/16.
 */


var tango = require("./tango.js");
var PrettyPrint = require("./pretty_print.js");
var _ = require("underscore");

var tangoOptions = {
    base_url: "https://api.tangocard.com",
    basic_auth: "UG9sYXJMYWJzOkdFN3pLR0ZSMlRIcHZxTVo5cjV3ZVpHSVZUM0tBSzRESmN0bXJHZndwR2FsUThJQTcyR2E1VnVXdw=="
};

var module = module.exports;

module.ShowTask = function(brand){
    return tango(tangoOptions).getCatalog().then(function(results){
        var matchedBrands = _.filter(results, function(b){
            return b.description.toLowerCase().indexOf(brand.toLowerCase()) != -1;
        });

        PrettyPrint.printBrands(matchedBrands);
        return matchedBrands;
    });
};
