var rp = require("request-promise");
var _ = require("underscore");

module.exports = function(options){

	options = options || {};

	var base_url = options.base_url || "https://sandbox.tangocard.com";
	var basic_auth = "Basic " + (options.basic_auth || "VGFuZ29UZXN0OjV4SXRyM2RNRGxFV0FhOVM0czd2WWg3a1EwMWQ1U0ZlUFBVb1paaUsvdk1mYm8zQTVCdkpMQW1ENHRJPQ==");

	return {
		getCatalog:function(){			
			var req_options = {
				url: base_url + "/raas/v1.1/rewards",
				method:"GET",
				json:true,
				headers:{
					"Content-Type": "application/json",
					"Accept": "*/*",
					"Authorization": basic_auth
				}
			};
			return rp(req_options).then(function(response){

				if(response.success){
					return response.brands;
				}else{
					[];
				}

			})
		}

	};

};