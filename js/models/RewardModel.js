/**
 * Created by arahman on 12/29/14.
 */
var Parse = require("parse/node");
var _ = require('underscore');
var Countries = require("../../../getgiftzserver/cloud/GiftModule/CountryData");
var Locale = require("../../../getgiftzserver/cloud/GiftModule/Locale");

var Model = Parse.Object.extend("Reward", {

}, {

    newReward: function (brand, reward_raw, brand_raw) {

        var countryNames = _
            .chain(reward_raw.countries)
            .map(function(c){
                return _.find(Countries, function(cc){
                    return cc.Code == c;
                });
            })
            .reject(function(c){
                return !c;
            })
            .map(function(c){
                if(reward_raw.is_variable == false){
                    return {
                        "activated": false,
                        "brandKey": brand.get("brandKey"),
                        "coins": 7500,
                        "country": c.Name,
                        "currencyCode": reward_raw.currency_code,
                        "currencyType": "USD",
                        "denomination": 2500,
                        "description": reward_raw.description,
                        "giftOrder": 20,
                        "imageUrl": brand_raw.image_url,
                        "info": brand,
                        "isVariablePriced": reward_raw.is_variable,
                        "locale": Locale.getLocale(cc.Code),
                        "sku":reward_raw.sku,
                        "tango_visible": reward_raw.available,
                        "unitPrice": 2500
                    };
                }
            })
            .value();


        var data = {
            "activated": false,
            "brandKey": brand.get("brandKey"),
            "coins": 7500,
            "country": "South Sudan",
            "currencyCode": "USD",
            "currencyType": "USD",
            "denomination": 2500,
            "description": reward.description,
            "giftOrder": 20,
            "imageUrl": brand_raw.image_url,
            "info": brand,
            "isVariablePriced": reward_raw.is_variable,
            "locale": "ar_SS",
            "objectId": "01n8aEQLAp",
            "sku": "PPVV-E-V-STD",
            "tango_visible": true,
            "unitPrice": 2500,
            "updatedAt": "2016-02-26T16:10:37.052Z"
        };

        return (new Model()).save(data);
    },
    query: function () {
        return new Parse.Query(Model);
    },
    getById: function (id) {
        return Model.query().get(id);
    },
    ifRewardExistsForSKU: function (sku) {
        return Model.query().equalTo("sku", sku).find(function (result) {
            return (result && result.length > 0);
        });
    },
    description: function () {

        return {

        };

    }
});

module.exports = Model;
