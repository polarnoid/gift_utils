/**
 * Created by arahman on 12/29/14.
 */
var Parse = require("parse/node");
var Model = Parse.Object.extend("BrandInfo",{
    updateBrand:function(brand_raw){
        return this.save(Model.brandRowFromRaw(brand_raw));
    }
}, {
    brandRowFromRaw:function(brand_raw){
        return {
            "activated": false,
            "appleEnable": false,
            "brandKey": brand_raw.description,
            "country": "USA",
            "disclaimer": "The listed merchants are in no way affiliated with Polar Labs UG nor are the listed merchants to be considered sponsors or co-sponsors of this program. Use of merchant names and/or logos is by the permission of each respective merchant and all trademarks are the property of their respective owners. Terms and conditions are applied to gift cards/certificates. Please see the merchant gift card/gift certificate for additional terms and conditions, which are subject to change at merchant’s sole discretion.",
            "name": brand_raw.description,
            "tango_visible": true
        };
    },
    newBrand:function(brand_raw){
      var data = Model.brandRowFromRaw();

        return (new Model()).save(data);
    },
    query: function(){
        return new Parse.Query(Model);
    },
    getById:function(id){
        return Model.query().get(id);
    },
    getByBrandName:function(name){
    	return Model.query().equalTo("brandKey", name).find();
    },
    description:function(){

    	return {

    	};

    }
});

module.exports = Model;
