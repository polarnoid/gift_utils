/**
 * Created by arahman on 5/22/16.
 */
var Parse = require("parse/node");
var BrandInfoModel = require("../js/models/BrandInfoModel.js");
var RewardModel = require("../js/models/RewardModel.js");
var chalk = require("chalk");
var _ = require("underscore");

var task_insert_update_brand = function(brand_raw){
    return BrandInfoModel.getByBrandName(brand_raw.description).then(function(b){
        if(b){
            console.log(chalk.bold.red("%s found in the parse table. resetting the brand...", b.get("brandKey")));
            return b.updateBrand(brand_raw);
        }
        console.log(chalk.bold.green("%s NOT FOUND in the parse table. creating a new brand", brand_raw.description));
        return BrandInfoModel.newBrand(brand_raw);

    });
};


var task_insert_update_reward = function(brand_save_task, reward_raw){
    return brand_save_task.then(function(b){
        return RewardModel
    })
};

module.exports = function (brand_raw, [rewards_raw]) {

    return BrandInfoModel.getByBrandName(brand_raw.description).then(function(b){
        if(b){
            console.log(chalk.bold.red("%s found in the parse table. using this brand %s", b.get("brandKey"), b.get("objectId")));
            return b;
        }
        console.log(chalk.bold.green("%s NOT found in the parse table. creating a new brand", brand_raw.description));
        return BrandInfoModel.newBrand(brand_raw);

    }).then(function(b){
        var p = Parse.Promise.as();
        _.each(rewards_raw, function(reward_raw){
            p.then(function(){
                return RewardModel.ifRewardExistsForSKU(reward_raw.sku).then(function(sku_exist){
                    if(sku_exist){
                        console.log(chalk.bold.red("%s sku already exists", reward_raw.sku));
                        return Parse.Promise.as();
                    }
                    console.log(chalk.bold.green("sku %s NOT found in the reward table. creating a new reward", reward_raw.sku));
                    return RewardModel.newReward(b, reward_raw, brand_raw);
                });
            });
        });
        return p;
    });

};