#!/usr/bin/sh

curl -X POST \
  -H "X-Parse-Application-Id: JKGlfg1mpdWGx9I5dDbTrifEwlK3OjTLqh4o763D" \
  -H "X-Parse-Master-Key: dHS7iBvXsfwAKigZKNJnjqXmj0SkIVskGoXRzVbL" \
  -H "Content-Type: application/json" \
  -d '{
        "requests": [
          {"method":"POST","path":"/1/classes/BrandInfo","body":{"activated":false,"appleEnable":false,"brandKey":"Tesco","country":"USA","disclaimer":"The listed merchants are in no way affiliated with Polar Labs UG nor are the listed merchants to be considered sponsors or co-sponsors of this program. Use of merchant names and/or logos is by the permission of each respective merchant and all trademarks are the property of their respective owners. Terms and conditions are applied to gift cards/certificates. Please see the merchant gift card/gift certificate for additional terms and conditions, which are subject to change at merchant’s sole discretion.","name":"Tesco","tango_visible":true}}
        ]
      }' \
  https://api.parse.com/1/batch