#!/usr/bin/bash

echo "module.exports = " > prod_catalog.js;

curl -X GET \
  -H "Authorization: Basic UG9sYXJMYWJzOkdFN3pLR0ZSMlRIcHZxTVo5cjV3ZVpHSVZUM0tBSzRESmN0bXJHZndwR2FsUThJQTcyR2E1VnVXdw==" \
  -H "Accept: */*" \
  -H "Content-Type: application/json" \
  https://api.tangocard.com/raas/v1.1/rewards \
  >> prod_catalog.js;
  echo ";" >> prod_catalog.js;
