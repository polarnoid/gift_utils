#!/usr/bin/env node

var program = require("commander");
var _ = require("underscore");
var co = require("co");
var prompt = require("co-prompt");
var fs = require("fs");

var ShowTask = require('./js/tasks').ShowTask;
var JS = require("format-json");

//Parse.initialize(config.app_id, config.rest_key, config.master_key);

var PromptFor = function (title, result_callback) {

    return co(function *() {
        var p = yield prompt(title);
        return result_callback(p);
    });
};


var toBrandRequestData = function(data){
    return {
        "method": "POST",
        "path": "/1/classes/BrandInfo",
        "body":{
            "activated": false,
            "appleEnable": false,
            "brandKey": data.name,
            "country": "USA",
            "disclaimer": data.disclaimer,
            "name": data.name,
            "tango_visible": true
        }
    };
};

var toRewardRequestData = function(data) {

    /*
    * "name": "Americanas.com",
     "image_url": "https://d30s7yzk2az89n.cloudfront.net/graphics/item-images/americanas-brl-gift-card.png",
     "denominations":[
     3000, 4000, 6000, 8000
     ],
     "rate": 3.956464,
     "sku_template": "NETS-E-{price}-STD",
     "gift_description_template": "Americanas.com Gift Card BRL {price}",
     "country": "Brazil",
     "locale": "pt_BR",
     "brand_object_id": "n5lAkjGsf0",
     "currency_code": "BRL"
    * */
    return {
        "brand": data.name,
        "body": _.map(data.denominations, (deno) => {
            deno = deno | 0;
            var price = Math.floor(deno / 100);
            var rate = Number(data.rate);
            var unitPrice = Math.floor(deno / rate);
            var coins = unitPrice * 3;

            return {
                "method": "POST",
                "path": "/1/classes/Reward",
                "body": {
                    "activated": true,
                    "brandKey": data.name,
                    "coins": coins,
                    "country": data.country,
                    "currencyCode": data.currency_code,
                    "currencyType": "USD",
                    "denomination": deno,
                    "description": data.gift_description_template.replace("{price}", price),
                    "giftOrder": 50,
                    "imageUrl": data.image_url,
                    "info": {
                        "__type": "Pointer",
                        "className": "BrandInfo",
                        "objectId": data.brand_object_id
                    },
                    "isVariablePriced": data.is_variable,
                    "locale": data.locale,
                    "sku": data.sku_template.replace("{deno}", deno),
                    "tango_visible": true,
                    "unitPrice": unitPrice
                }
            }
        })
    };
};


program
    .command("post_data")
    .option("-f, --file <file>", "input json file")
    .action(function(options){
        var file_name = options.file;
        var input = JSON.parse(fs.readFileSync(file_name));

        var post_data = _.map(input.brands, toRewardRequestData);

        console.log(JS.diffy(post_data));
    });

program
    .command("show")
    .option("-b, --brand <brand>", "show only this brand")
    .action(function (options) {
        var brand = options.brand;
        ShowTask(brand);
    });

program.parse(process.argv);
